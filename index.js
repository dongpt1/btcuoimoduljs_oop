// Class riêng cho đối tượng user
class User {
    id
    name;

    constructor(name, id) {
        this.id = id;
        this.name = name;
    }

    getName() {
        return this.name;
    }

    getId() {
        return this.id;
    }

    setName(name) {
        this.name = name;
    }
}

// class quản lý
class UserManager {
    listUsers;

    constructor() {
        this.listUsers = [];
    }

    // Thêm người dùng mới
    addUser() {
        //lấy ra đối tượng ô input
        let inputAddObject = document.getElementById('input_name');

        // lấy ra tên ô input nhập tên mới
        let newName = inputAddObject.value;

        // kiểm tra người dùng đã nhập tên chưa
        if(newName === ''){
            alert("Mời nhập tên người dùng mới");
            return;
        }

        // tạo ra đối tượng user mới
        // với id ta sẽ lấy length để thành id tăng dần
        let newUser = new User(newName, this.listUsers.length);

        // thêm đối tượng mới vào danh sách user
        this.listUsers.push(newUser);

        // xóa dữ liệu cũ ở ô input
        inputAddObject.value = '';

        // vẽ lại table
        this.redrawTable();
    }

    // tìm kiếm người dùng theo tên (keywork tìm kiếm)
    searchUserByName(){
        // lấy tên cần tím kiếm
        let nameSearch = document.getElementById('input_search').value;
        let listUsersSearch = [];
        for (let index = 0; index < this.listUsers.length; index++) {
            let user = this.listUsers[index];
            if(user.getName().includes(nameSearch)){
                listUsersSearch.push(user);
            }
        }
        // vẽ lại table
        this.redrawTable(listUsersSearch, true);
    }

    // Xóa người dùng
    // Tham số là id của user trong danh sách
    deleteUserById(id){
        for (let i = 0; i < this.listUsers.length; i++) {
            let user = this.listUsers[i];
            if(user.id === id){
                this.listUsers.splice(i, 1);
                break;
            }
        }
        // vẽ lại table sau khi xóa user thành công
        this.redrawTable();
    }

    // Sửa tên người dùng theo id
    editUser(id) {
        let userObject = this.getUserAndIndexById(id);
        let userEdit = userObject.user;

        let nameEdit = prompt('Bạn muốn sửa tên người dùng ' + userEdit.getName())
        // set lại name mới
        userEdit.setName(nameEdit);
        
        // gán lại user trong danh sách
        this.listUsers[userObject.index] = userEdit;
        this.redrawTable();
    }

    // vẽ lại body của table
    // isSearch là check việc search hay hiển thị bình thường
    redrawTable(listUserSearch = [], isSearch = false) {
        //lấy ra đối tượng body;
        let tableObject = document.getElementById('table_body');

        // danh sách user cần hiển thị
        let listUserView = [];

        // Khai báo nội dung ban đầu trong body của table
        let innerHtmlBodyTable = '';

        // check xem là vẽ lại danh sách hay dùng toàn bộ danh sách
        if (isSearch) {
            listUserView = listUserSearch;
        } else {
            listUserView = this.listUsers;
        }

        // tương ứng với một hàng thông tin sẽ là 1 user
        // nên chúng ta sẽ for để có tất cả các thông tin
        for (let i = 0; i < listUserView.length; i++) {
            // lấy ra thông tin tại vị trí i
            let user = listUserView[i];

            // html table cho 1 thông tin user
            let htmlRow = `<tr>
                                <td>${i+1}</td>
                                <td>${user.getName()}</td>
                                <td><button onclick="userManager.editUser(${user.getId()})">EDIT</button></td>
                                <td><button onclick="userManager.deleteUserById(${user.getId()})">DELETE</button></td>
                            </tr>`;
            // ghép html từng dòng lại với nhau
            innerHtmlBodyTable += htmlRow;
        }

        // set lại nội dung bên trong body table (vẽ lại table)
        tableObject.innerHTML = innerHtmlBodyTable;
    }

    // lấy ra người dùng và vị trí của người dùng theo id
    // format object {
    //     'user': user
    //     'index': index
    // }
    getUserAndIndexById(id){
        for (let i = 0; i < this.listUsers.length; i++) {
            let user = this.listUsers[i];
            if(user.id === id){
                return {
                        'user': user,
                        'index': i
                };
            }
        }
    }
}

// khởi tạo đối tượng để đưa function vào onclick của button
let userManager = new UserManager();